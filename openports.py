'''
Author: Ricardo Castro
Resources: Stackoverflow

Description:
This program check for open ports.

Thoughts:

'''
import socket
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.settimeout(2)                                      #2 Second Timeout

def socket_loop(port):
    ip = raw_input("Enter IP address: ")
    result = sock.connect_ex((ip, int(port)))
    if result == 0:
        print 'port OPEN'
    else:
        print 'port CLOSED, connect_ex returned: '+str(result)
def get_port():
    port = raw_input("Enter port: ")
    socket_loop(port)
def all_ports():
    p = list(range(0,6535))
    for port in p:
        print "Checking Port : {0}".format(port)
        socket_loop(port)
def common_ports():
    commonPorts = {21, 22, 23, 25, 53, 80, 110, 119, 123, 143, 161, 194, 443}
    for ports in commonPorts:
        socket_loop(ports)

def main():
    options = {"-a","-p","-c"}

    if len(sys.argv) == 1:
        display()
    elif len(sys.argv) == 2:
        if sys.argv[1] == '-a':
            all_ports()
        elif sys.argv[1] == '-c':
            common_ports()
    elif len(sys.argv) >= 3:
        socket_loop(sys.argv[2])

def display():
    version = 0.3
    print "Open Port Finder v{0}".format(version)
    print "Format: {0} [option] [port] [ip]".format(sys.argv[0])
    print "Options:---------------------------------"
    print "a Use the program on all ports."
    print "p Specify port."
    print "c Use on common ports."

if __name__ == '__main__':
    main()
